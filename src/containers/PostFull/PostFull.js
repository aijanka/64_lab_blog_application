import React, {Component} from 'react';
import {Button, Grid, Jumbotron} from "react-bootstrap";
import axios from 'axios';
import {NavLink} from "react-router-dom";

class PostFull extends Component {

    state = {
        post: {
            body: '',
            title: ''
        }
    };

    id = this.props.match.params.id;

    getPost () {
        axios.get(`/posts/${this.id}.json`).then(response => {
            console.log(response.data);
            this.setState({post: response.data});

        })
    }

    componentDidMount () {
        this.getPost();
    }

    deletePost = () => {
        axios.delete(`/posts/${this.id}.json`).then(() => {
            this.getPost();
            this.props.history.replace('/');
        })
    };


    render () {
        const link = `/posts/${this.id}/edit`;
        return (
            <Grid>
                <Jumbotron>
                    <h2>{this.state.post.title}</h2>
                    <p>{this.state.post.body}</p>
                    <p>
                        <Button bsStyle="primary" onClick={this.deletePost}>Delete</Button>
                        <Button bsStyle="primary"><NavLink to={link}>Edit</NavLink></Button>
                    </p>
                </Jumbotron>
            </Grid>
        )
    }

};

export default PostFull;