import React, {Component} from 'react';
import axios from 'axios';
import {Grid} from "react-bootstrap";
import InputPostForm from "../../components/InputPostForm/InputPostForm";
import * as EditorState from "draft-js";

class PostEdit extends Component {
    state = {
        post: {
            title: '',
            // body: EditorState.createEmpty()
        }
    };

    id = this.props.match.params.id;

    componentDidMount () {
        axios.get(`/posts/${this.id}.json`).then(response => {
            this.setState({post: response.data});
        })

    }

    saveCurrentValue = event => {
        const post = {...this.state.post};
        post[event.target.name] = event.target.value;
        this.setState({post});
        console.log(this.state.post);
    };

    editPost = (event) => {
        event.preventDefault();
        axios.patch(`/posts/${this.id}.json`, this.state.post).then(() => {
            this.props.history.replace('/');
        })
    }

    render() {
        return (
            <Grid>
                <InputPostForm
                    typed={this.saveCurrentValue}
                    titleValue={this.state.post.title}
                    bodyValue={this.state.post.body}
                    submit={this.editPost}
                />

            </Grid>
        )
    }
}

export default PostEdit;