import React, {Component} from 'react';
import {Grid} from "react-bootstrap";
import axios from 'axios';
import PostShort from "../../components/PostShort/PostShort";

class PostsList extends Component {
    state = {
        posts: []
    };

    componentDidMount() {
        this.getPosts();
    }

    getPosts() {
        axios.get('/posts.json').then(response => {
            this.setState({posts: response.data});
        })
    }

    moveToBigDesc(id) {
        this.props.history.replace(`/posts/${id}`);
    }

    render() {
        if (this.state.posts) {
            const keys = Object.keys(this.state.posts);
            return (
                <Grid>
                    {keys.map(id => {
                        const post = this.state.posts[id];
                        return (<PostShort
                                    // date={post.date}
                                    key={id}
                                    id={id}
                                    title={post.title}
                                    body={post.body}
                                    more={() => this.moveToBigDesc(id)}
                                />);
                                                })
                    }
                </Grid>
            )
        } else {
            return <h1>No posts yet</h1>;
        }
    }
};

export default PostsList;

