import React, {Component} from 'react';
import {Grid} from "react-bootstrap";
import axios from 'axios';
import InputPostForm from "../../components/InputPostForm/InputPostForm";

class PostAdd extends Component {
    state = {
        post: {
            title: '',
            body: ''
        }

    };

    saveCurrentValue = event => {
        const post = {...this.state.post};
        post[event.target.name] = event.target.value;
        this.setState({post});
    };

    saveFullPost = event => {
        event.preventDefault();
        axios.post('/posts.json', this.state.post).then(() => {
            this.props.history.replace('/');
        })
    };


    render() {
        return (
            <Grid>
                <InputPostForm
                    typed={this.saveCurrentValue}
                    submit={this.saveFullPost}
                />
            </Grid>
        )
    }
}

export default PostAdd;