import React from 'react';
import {Button, Jumbotron} from "react-bootstrap";
import {NavLink} from "react-router-dom";
// import Date from 'date-now';

const PostShort = props => {
    const link = `/posts/${props.id}`;
    return (
        <Jumbotron>
            <h3>{props.title}</h3>
            {/*<p>{props.date}</p>*/}
            <p>
                <Button><NavLink to={link}>Read more >> </NavLink></Button>
            </p>
        </Jumbotron>
    )
};

export default PostShort;